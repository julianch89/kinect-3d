#pragma once
#include "Filter.h"
#include <list>

template <typename PointType>
class FilterSmooth : public Filter <PointType>
{
public:
		
	
	FilterSmooth(int m=0)
	{
		method = m;
	}

	~FilterSmooth(void)
	{
	}
	int method;
	void aplyFilter(CloudPtr cloud)
	{
		int h=cloud->height;
		int w=cloud->width;
		CloudPtr cloudAux(new Cloud);
		cloudAux->width=w;
		cloudAux->height=h;
		cloudAux->is_dense = false;
		cloudAux->points.resize (cloud->width * cloud->height);
			
		for (int y=0; y<h; y++)
		{
			for(int x=0;x<w;x++)
			{
				PointType p= cloud->at(x,y);
				cloudAux->at(x,y)=cloud->at(x,y);	
				if(pcl_isfinite(p.z) )
				{
					
					
					if (method == 0) cloudAux->at(x,y).z=smoothPixel(x,y,cloud);
					if (method == 1) cloudAux->at(x,y).z=smoothPixelAlt(x,y,cloud,true);
					if (method == 2) cloudAux->at(x,y).z=smoothPixelAlt(x,y,cloud,false);
				}
					
			}
		}
		*cloud=*cloudAux;
	}
private : 

	double smoothPixel(int x, int y, CloudPtr cloud)
        {
			double limit=0.1;
			double depth= cloud->at(x,y).z;
            double suma = 0;
            double pixels = 0;
				
            if (x > 0)
            {
				if (pcl_isfinite(cloud->at(x-1,y).z) && std::abs(depth-cloud->at(x-1,y).z) < limit)
                {
                    suma += cloud->at(x-1,y).z;
                    pixels++;
                }

                if (y > 0 )
                {
                    if (pcl_isfinite(cloud->at(x-1,y-1).z ) && std::abs(depth-cloud->at(x-1,y-1).z) < limit)
                    {
                        suma +=cloud->at(x-1,y-1).z;
                        pixels++;
                    }
                }
                if (y < 479 )
                {
                    if (pcl_isfinite(cloud->at(x-1,y+1).z )&& std::abs(depth-cloud->at(x-1,y+1).z) < limit)
                    {
                        suma += cloud->at(x-1,y+1).z;
                        pixels++;
                    }
                }
            }
            if (x < 639)
            {

                if (pcl_isfinite(cloud->at(x+1,y).z )&& std::abs(depth-cloud->at(x+1,y).z) < limit)
                {

                    suma += cloud->at(x+1,y).z;
                    pixels++;
                }
                if (y < 479 )
                {
                    if (pcl_isfinite(cloud->at(x+1,y+1).z )&& std::abs(depth-cloud->at(x+1,y+1).z) < limit)
                    {
                        suma += cloud->at(x+1,y+1).z;
                        pixels++;
                    }
                }
                if (y > 0)
                {
                    if (pcl_isfinite(cloud->at(x+1,y-1).z )&& std::abs(depth-cloud->at(x+1,y-1).z) < limit)
                    {
                        suma += cloud->at(x+1,y-1).z;
                        pixels++;
                    }
                }

            }

            if (y > 0)
            {
                if (pcl_isfinite(cloud->at(x,y-1).z )&& std::abs(depth-cloud->at(x,y-1).z) < limit)
                {
                    suma += cloud->at(x,y-1).z;
                    pixels++;
                }
            }
            if (y < 479)
            {
                if (pcl_isfinite(cloud->at(x,y+1).z )&& std::abs(depth-cloud->at(x,y+1).z) < limit)
                {
                    suma += cloud->at(x,y+1).z;
                    pixels++;
                }
            }

            suma += cloud->at(x,y).z;
            pixels++;
			return suma / pixels; 
        }

	double smoothPixelAlt(int x, int y, CloudPtr cloud , bool method)
        {
			double limit=0.3;
			double depth= cloud->at(x,y).z;
            std::list<double> neighbours;
			
				
            if (x > 0)
            {
				if (pcl_isfinite(cloud->at(x-1,y).z) && std::abs(depth-cloud->at(x-1,y).z) < limit)
                {
					neighbours.push_front(cloud->at(x-1,y).z);
                }

                if (y > 0 )
                {
                    if (pcl_isfinite(cloud->at(x-1,y-1).z ) && std::abs(depth-cloud->at(x-1,y-1).z) < limit)
                    {
                        neighbours.push_front(cloud->at(x-1,y-1).z);
                    }
                }
                if (y < 479 )
                {
                    if (pcl_isfinite(cloud->at(x-1,y+1).z )&& std::abs(depth-cloud->at(x-1,y+1).z) < limit)
                    {
                        neighbours.push_front(cloud->at(x-1,y+1).z);
                    }
                }
            }
            if (x < 639)
            {

                if (pcl_isfinite(cloud->at(x+1,y).z )&& std::abs(depth-cloud->at(x+1,y).z) < limit)
                {

                    neighbours.push_front(cloud->at(x+1,y).z);
                }
                if (y < 479 )
                {
                    if (pcl_isfinite(cloud->at(x+1,y+1).z )&& std::abs(depth-cloud->at(x+1,y+1).z) < limit)
                    {
                        neighbours.push_front(cloud->at(x+1,y+1).z);
                    }
                }
                if (y > 0)
                {
                    if (pcl_isfinite(cloud->at(x+1,y-1).z )&& std::abs(depth-cloud->at(x+1,y-1).z) < limit)
                    {
                        neighbours.push_front(cloud->at(x+1,y-1).z);
                    }
                }

            }

            if (y > 0)
            {
                if (pcl_isfinite(cloud->at(x,y-1).z )&& std::abs(depth-cloud->at(x,y-1).z) < limit)
                {
                    neighbours.push_front(cloud->at(x,y-1).z);
                }
            }
            if (y < 479)
            {
                if (pcl_isfinite(cloud->at(x,y+1).z )&& std::abs(depth-cloud->at(x,y+1).z) < limit)
                {
                    neighbours.push_front(cloud->at(x,y+1).z);
                }
            }

			
			//if(method)
			//{
				int mid = neighbours.size()/2;
			
				neighbours.sort();
				
				return 0;
			//}
			//else
			//{
				

			//}
        }

};

