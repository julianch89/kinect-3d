#pragma once
#include <pcl\point_types.h>
template <typename PointType>
class Filter
{
public:
	typedef pcl::PointCloud<PointType> Cloud;
    typedef typename Cloud::Ptr CloudPtr;
    typedef typename Cloud::ConstPtr CloudConstPtr;

	Filter(void)
	{
	}
	virtual void aplyFilter(CloudPtr cloud) = 0;
	~Filter(void)
	{
	}
};

