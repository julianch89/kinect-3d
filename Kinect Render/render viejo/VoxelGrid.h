#include <string>


template <typename PointType>
class OpenNIVoxelGrid
{
  public:
    typedef pcl::PointCloud<PointType> Cloud;
    typedef typename Cloud::Ptr CloudPtr;
    typedef typename Cloud::ConstPtr CloudConstPtr;
	typedef pcl::PointNormal PointNormalT;
	typedef pcl::PointCloud<PointNormalT> PointCloudWithNormals;


    OpenNIVoxelGrid (const std::string& device_id = "", 
                     const std::string& = "z", float = 0, float = 5.0,
                     float leaf_size_x = 0.01, float leaf_size_y = 0.01, float leaf_size_z = 0.01)
    : 
	viewer  (new pcl::visualization::PCLVisualizer ("Render Kinect"))
	, viewer_join  (new pcl::visualization::PCLVisualizer ("Render Final"))
	, cloud_total(new Cloud)
    {
      grid_.setLeafSize (leaf_size_x, leaf_size_y, leaf_size_z);
	  viewer->setBackgroundColor (0.1, 0.1, 0.1);
	  viewer->initCameraParameters(); 
	  viewer->addCoordinateSystem (1.0);
	  viewer_join->setBackgroundColor (0.1, 0.1, 0.1);
	  viewer_join->initCameraParameters(); 
	  viewer_join->addCoordinateSystem (1.0);
      first_time=true;
    }
    
    void cloud_cb_ (const CloudConstPtr& cloud)
    {
      set (cloud);
    }

    void set (const CloudConstPtr& cloud)
    {
      //lock while we set our cloud;
      boost::mutex::scoped_lock lock (mtx_);
      cloud_  = cloud;
    }

	CloudPtr getFiltered(){
	   boost::mutex::scoped_lock lock (mtx_);
	   std::vector<int> indices;
	   CloudPtr temp (new Cloud);
       pcl::removeNaNFromPointCloud(*cloud_,*temp, indices);
	   return (temp);
	}

    CloudPtr get ()
    {
      //lock while we swap our cloud and reset it.
      boost::mutex::scoped_lock lock (mtx_);
      CloudPtr temp_cloud (new Cloud);
     
      grid_.setInputCloud (cloud_);
      grid_.filter (*temp_cloud);

      return (temp_cloud);
    }
	void pairAlign (const CloudPtr cloud_src, const CloudPtr cloud_tgt, CloudPtr output, Eigen::Matrix4f &final_transform, bool downsample = false)
	{
	  //
	  // Downsample for consistency and speed
	  // \note enable this for large datasets
	  CloudPtr src (new Cloud);
	  CloudPtr tgt (new Cloud);
	  pcl::VoxelGrid<PointType> grid;
	  if (downsample)
	  {
		grid.setLeafSize (0.05, 0.05, 0.05);
		grid.setInputCloud (cloud_src);
		grid.filter (*src);

		grid.setInputCloud (cloud_tgt);
		grid.filter (*tgt);
	  }
	  else
	  {
		src = cloud_src;
		tgt = cloud_tgt;
	  }


	  // Compute surface normals and curvature
	  PointCloudWithNormals::Ptr points_with_normals_src (new PointCloudWithNormals);
	  PointCloudWithNormals::Ptr points_with_normals_tgt (new PointCloudWithNormals);

	  pcl::NormalEstimation<PointType, PointNormalT> norm_est;
	  pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ> ());
	  norm_est.setSearchMethod (tree);
	  norm_est.setKSearch (30);
  
	  norm_est.setInputCloud (src);
	  norm_est.compute (*points_with_normals_src);
	  pcl::copyPointCloud (*src, *points_with_normals_src);

	  norm_est.setInputCloud (tgt);
	  norm_est.compute (*points_with_normals_tgt);
	  pcl::copyPointCloud (*tgt, *points_with_normals_tgt);

	  
	  //
	  // Align
	  pcl::IterativeClosestPointNonLinear<PointNormalT, PointNormalT> reg;
	  reg.setTransformationEpsilon (1e-6);
	  // Set the maximum distance between two correspondences (src<->tgt) to 10cm
	  // Note: adjust this based on the size of your datasets
	  reg.setMaxCorrespondenceDistance (0.05);  
	  // Set the point representation
	  

	  reg.setInputCloud (points_with_normals_src);
	  reg.setInputTarget (points_with_normals_tgt);



	  //
	  // Run the same optimization in a loop and visualize the results
	  Eigen::Matrix4f Ti = Eigen::Matrix4f::Identity (), prev, targetToSource;
	  PointCloudWithNormals::Ptr reg_result = points_with_normals_src;
	  reg.setMaximumIterations (60);

		// Estimate
	  
	  reg.align (*reg_result);

			//accumulate transformation between each Iteration
	  Ti = reg.getFinalTransformation () * Ti;

		
	  // Get the transformation from target to source
	  targetToSource = Ti.inverse();

	  
	  // Transform target back in source frame
	  pcl::transformPointCloud (*cloud_tgt, *output, targetToSource);

	 
	  //add the source to the transformed target
	  *output += *cloud_src;
  
	  final_transform = targetToSource;
	  viewer_join->removeAllPointClouds();
	  viewer_join->addPointCloud(output,"total");
	 }

	bool first_time;
	void keyboard_callback (const pcl::visualization::KeyboardEvent& event, void* cookie)
    {
      if(event.keyDown())
	  {
		  if (event.getKeyCode() == 32  ){
			boost::mutex::scoped_lock lock (mtx_);
			if(cloud_)
			{
				if(first_time){
					std::vector<int> indices;
					pcl::removeNaNFromPointCloud(*cloud_,*cloud_total, indices);
					
					first_time=false;
				}
				else{
					
					CloudPtr temp (new Cloud);
					CloudPtr aux (new Cloud);
					std::vector<int> indices;
					pcl::removeNaNFromPointCloud(*cloud_,*aux, indices);
					Eigen::Matrix4f pairTransform;
					pairAlign (aux, cloud_total, temp, pairTransform,true);
					*cloud_total=*temp;
				}
				
			}
		
		  }
		 //viewer_join->removeAllPointClouds();
		 //viewer_join->addPointCloud(cloud_total,"total");
		 viewer_join->spin();
	  }
    }
    void run ()
    {
      pcl::Grabber* interface = new pcl::OpenNIGrabber ();

	  
	  
	  viewer->registerKeyboardCallback(&OpenNIVoxelGrid::keyboard_callback, *this, (void*)NULL);
	  
	  
	  

      boost::function<void (const CloudConstPtr&)> f = boost::bind (&OpenNIVoxelGrid::cloud_cb_, this, _1);
      boost::signals2::connection c = interface->registerCallback (f);
      
      interface->start ();
      
      while (!viewer->wasStopped ())
      {
        if (cloud_)
        {
	      viewer->removePointCloud("depth cloud");
		  viewer->addPointCloud(get(),"depth cloud");
		 
		  viewer->spinOnce (100);
		 
        }
      }

      interface->stop ();
    }

    pcl::VoxelGrid<PointType> grid_;

	
	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer_join;

    boost::mutex mtx_;
    CloudConstPtr cloud_;
	CloudPtr cloud_total;
};