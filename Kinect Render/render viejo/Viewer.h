#pragma once
#include <pcl/visualization/pcl_visualizer.h>

template <typename PointType>
class Viewer
{
public:
	typedef pcl::PointCloud<PointType> Cloud;
    typedef typename Cloud::Ptr CloudPtr;
    typedef typename Cloud::ConstPtr CloudConstPtr;

	Viewer(void): viewer  (new pcl::visualization::PCLVisualizer ("Render Kinect"))
	{
		

		viewer->createViewPort (0.0, 0.0, 0.5, 1.0, viewport_input_);
        viewer->setBackgroundColor (0, 0, 0, viewport_input_);
        viewer->createViewPort (0.5, 0.0, 1.0, 1.0, viewport_result_);
        viewer->setBackgroundColor (0, 0, 0, viewport_result_);

		viewer->initCameraParameters(); 
		viewer->addCoordinateSystem (1.0);


	}
	~Viewer(void)
	{

	}
	/*
	void registerKeyboardCallBack(void(T::*callback) (const pcl::visualization::KeyboardEvent& func,void* cookie) f)
	{
		//viewer->registerKeyboardCallback(
	}*/

	void spin(int n)
	{
		viewer->spinOnce(n);
	}

	bool wasStopped()
	{
		return viewer->wasStopped();
	}
	void addCloud(CloudPtr c,string name, bool result )
	{
		
		if(!viewer->wasStopped())
			if(!viewer->updatePointCloud(c,name))
				if(!result) viewer->addPointCloud(c,name,viewport_input_);
				else viewer->addPointCloud(c,name,viewport_result_);
				
	}
	


	int viewport_input_, viewport_result_;
	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
};

