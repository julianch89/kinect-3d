#include <string>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/passthrough.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/surface/mls.h>
#include <boost/thread/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/openni_grabber.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/io/openni_camera/openni_driver.h>
#include <pcl/filters/approximate_voxel_grid.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/console/parse.h>
#include <pcl/common/time.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/icp_nl.h>
#include <pcl/registration/transforms.h>
#include <pcl/features/normal_3d.h>
#include <pcl/surface/mls.h>
#include <pcl/kdtree/kdtree_flann.h>

template <typename PointType>
class OpenNIVoxelGrid
{
  public:
    typedef pcl::PointCloud<PointType> Cloud;
    typedef typename Cloud::Ptr CloudPtr;
    typedef typename Cloud::ConstPtr CloudConstPtr;
	typedef pcl::PointNormal PointNormalT;
	typedef pcl::PointCloud<PointNormalT> PointCloudWithNormals;


    OpenNIVoxelGrid (const std::string& device_id = "", 
                     const std::string& = "z", float = 0, float = 5.0,
                     float leaf_size_x = 0.01, float leaf_size_y = 0.01, float leaf_size_z = 0.01)
    : 
	//viewer  (new pcl::visualization::PCLVisualizer ("Render Kinect"))
	//viewer ("PCL Viewer") 
	viewer  (new pcl::visualization::PCLVisualizer ("Render Kinect"))
	//, viewer_join  (new pcl::visualization::PCLVisualizer ("Render Final"))
	, result(new Cloud)
	, source (new Cloud)
    {
      grid_.setLeafSize (leaf_size_x, leaf_size_y, leaf_size_z);
	  viewer->setBackgroundColor (0.1, 0.1, 0.1);
	  viewer->initCameraParameters(); 
	  viewer->addCoordinateSystem (1.0);
	 // viewer_join->setBackgroundColor (0.1, 0.1, 0.1);
	 // viewer_join->initCameraParameters(); 
	 // viewer_join->addCoordinateSystem (1.0);
	  regis_start=false;
      
    }
    
    void cloud_cb_ (const CloudConstPtr& cloud)
    {
      //set (cloud);
	  if(!viewer->wasStopped())
	  {
		  if(source->empty())
		  {
			  std::vector<int> indices;
			  *source=*cloud;
			  //pcl::removeNaNFromPointCloud(*source,*source, indices);
		  }
		  if(regis_start)
		  {
			  CloudPtr temp (new Cloud);
			  CloudPtr target (new Cloud);
			  CloudPtr aux (new Cloud);	
					
			  *target =*cloud;
			  /*
			  pcl::StatisticalOutlierRemoval<PointType> sor;
			  sor.setInputCloud (source);
			  sor.setMeanK (50);
			  sor.setStddevMulThresh (1.0);
			  sor.filter (*source);
			  sor.setInputCloud (target);
			  sor.filter (*target);
			   */
			  /*
			  pcl::PassThrough<PointType> pass;
			  pass.setFilterFieldName ("z");
			  pass.setFilterLimits (0.0, 2.0);
			  
			  std::vector<int> indices;
			  pcl::removeNaNFromPointCloud(*target,*target, indices);*/

			  pairAlign (source, target, temp, pairTransform,true);
			  pcl::transformPointCloud (*temp, *temp, GlobalTransform); 
              
			  *source = *target; 
 			  

 			  GlobalTransform = pairTransform * GlobalTransform;
			  
			 // pass.setInputCloud (temp);
			 // pass.filter (*temp);

			  *result += *temp;
			  /*
			  pcl::search::KdTree<PointType>::Ptr tree (new pcl::search::KdTree<PointType>);
			  
			  pcl::MovingLeastSquares<PointType, PointType> mls;
 
			  mls.setComputeNormals (true);

			  // Set parameters
			  mls.setInputCloud (result);
			  mls.setPolynomialFit (true);
			  mls.setSearchMethod (tree);
			  mls.setSearchRadius (0.03);

			  // Reconstruct
			  mls.process (*aux);*/
			  
			  if(!viewer->updatePointCloud(result,"result"))
			  {
				  viewer->addPointCloud(result,"result");
			  }
			 // viewer->spin();*/
			 // viewer.showCloud(result); 
			  
			  regis_start=false;
		  }
	  }

    }

    void set (const CloudConstPtr& cloud)
    {
      //lock while we set our cloud;
      boost::mutex::scoped_lock lock (mtx_);
      cloud_  = cloud;
    }

	

    CloudPtr get ()
    {
      //lock while we swap our cloud and reset it.
      boost::mutex::scoped_lock lock (mtx_);
      CloudPtr temp_cloud (new Cloud);
     
      grid_.setInputCloud (cloud_);
      grid_.filter (*temp_cloud);

      return (temp_cloud);
    }
	void pairAlign (const CloudPtr cloud_src, const CloudPtr cloud_tgt, CloudPtr output, Eigen::Matrix4f &final_transform, bool downsample = false)
	{
	
	  CloudPtr src (new Cloud);
	  CloudPtr tgt (new Cloud);
	  pcl::VoxelGrid<PointType> grid;
	  if (downsample)
	  {
		grid.setLeafSize (0.05, 0.05, 0.05);
		grid.setInputCloud (cloud_src);
		grid.filter (*src);

		grid.setInputCloud (cloud_tgt);
		grid.filter (*tgt);
	  }
	  else
	  {
		src = cloud_src;
		tgt = cloud_tgt;
	  }


	  // Compute surface normals and curvature
	  PointCloudWithNormals::Ptr points_with_normals_src (new PointCloudWithNormals);
	  PointCloudWithNormals::Ptr points_with_normals_tgt (new PointCloudWithNormals);

	  pcl::NormalEstimation<PointType, PointNormalT> norm_est;
	  pcl::search::KdTree<PointType>::Ptr tree (new pcl::search::KdTree<PointType> ());
	  norm_est.setSearchMethod (tree);
	  norm_est.setKSearch (30);
  
	  norm_est.setInputCloud (src);
	  norm_est.compute (*points_with_normals_src);
	  pcl::copyPointCloud (*src, *points_with_normals_src);

	  norm_est.setInputCloud (tgt);
	  norm_est.compute (*points_with_normals_tgt);
	  pcl::copyPointCloud (*tgt, *points_with_normals_tgt);

	  
	  //
	  // Align
	  pcl::IterativeClosestPointNonLinear<PointNormalT, PointNormalT> reg;
	  reg.setTransformationEpsilon (1e-8);
	  // Set the maximum distance between two correspondences (src<->tgt) to 10cm
	  // Note: adjust this based on the size of your datasets
	  reg.setMaxCorrespondenceDistance (1.0);  
	  // Set the point representation
	  

	  reg.setInputCloud (points_with_normals_src);
	  reg.setInputTarget (points_with_normals_tgt);



	  //
	  // Run the same optimization in a loop and visualize the results
	  Eigen::Matrix4f Ti = Eigen::Matrix4f::Identity (), targetToSource;
	  PointCloudWithNormals::Ptr reg_result = points_with_normals_src;
	  reg.setMaximumIterations (60);

		// Estimate
	  
	  reg.align (*reg_result);

			//accumulate transformation between each Iteration
	  Ti = reg.getFinalTransformation ();

		
	  // Get the transformation from target to source
	  targetToSource = Ti.inverse();

	  
	  // Transform target back in source frame
	  pcl::transformPointCloud (*cloud_tgt, *output, targetToSource);

	 
	  //add the source to the transformed target
	  *output += *cloud_src;
	  
	  final_transform = targetToSource;
	  
	 }

	bool regis_start;
	void keyboard_callback (const pcl::visualization::KeyboardEvent& event, void* cookie)
    {
      if(event.keyDown())
	  {
		  if (event.getKeyCode() == 32  )
		  {
			  regis_start=true;
		  }
	  }
	  
    }
    void run ()
    {
      pcl::Grabber* interface = new pcl::OpenNIGrabber ();

	  GlobalTransform = Eigen::Matrix4f::Identity (); 
	  
	  viewer->registerKeyboardCallback(&OpenNIVoxelGrid::keyboard_callback, *this, (void*)NULL);
	  
	  
	  

      boost::function<void (const CloudConstPtr&)> f = boost::bind (&OpenNIVoxelGrid::cloud_cb_, this, _1);
      boost::signals2::connection c = interface->registerCallback (f);
      
      interface->start ();
      
      while (!viewer->wasStopped ())
      {
		viewer->spinOnce(100);
         
      }

      interface->stop ();
    }

    pcl::VoxelGrid<PointType> grid_;

	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
	//boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer_join;
	Eigen::Matrix4f GlobalTransform;
	Eigen::Matrix4f pairTransform;
    boost::mutex mtx_;
    CloudConstPtr cloud_;
	CloudPtr result;
	CloudPtr source;
   // pcl::visualization::CloudViewer viewer; 
};