
#include <iostream>
#include <string>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/io/openni_grabber.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/compression/octree_pointcloud_compression.h>

#include <boost/make_shared.hpp>
#include <pcl/point_cloud.h>
#include <pcl/point_representation.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/filter.h>
#include <pcl/features/normal_3d.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/icp_nl.h>
#include <pcl/registration/transforms.h>
#include <pcl/features/pfh.h>
#include <pcl/features/fpfh.h>
#include "pcl/kdtree/kdtree_flann.h" 
#include <pcl/filters/passthrough.h>
#include <pcl/registration/ia_ransac.h>
#include <pcl/filters/statistical_outlier_removal.h>



#ifdef _WIN32 
# define sleep(x) Sleep((x)*1000) 
#endif 

//#define  FPFH_METHOD 


//ICP alignment parameters 
const int ICP_MAX_ITERATIONS = 50; 
const double ICP_MAX_CORRESPONDENCE_DISTANCE = 0.25; 
const double ICP_TRANSFORMATION_EPSILON = 1e-8; 
const double ICP_EUCLIDEAN_FITNESS_EPSILON = 1e-5; 

//FPFH feature parameters 
const double FEATURES_RADIUS = 0.3; 

//FPFH alignment parameters 
const double SAC_MAX_CORRESPONDENCE_DIST = 0.05; 
const double SAC_MIN_SAMPLE_DIST = 0.5; 
const int SAC_MAX_ITERATIONS = 1000; 


//convenient typedefs 
typedef pcl::PointXYZRGBA PointT; 
typedef pcl::PointCloud<PointT> PointCloud; 
typedef pcl::PointNormal PointNormalT; 
typedef pcl::PointCloud<PointNormalT> PointCloudWithNormals; 



class MyPointRepresentation : public pcl::PointRepresentation <PointNormalT>
{ 
        using pcl::PointRepresentation<PointNormalT>::nr_dimensions_; 
public: 
        MyPointRepresentation () 
        { 
                // Define the number of dimensions 
                nr_dimensions_ = 4; 
        } 

        // Override the copyToFloatArray method to define our feature vector 
        virtual void copyToFloatArray (const PointNormalT &p, float * out) const 
        { 
                // < x, y, z, curvature > 
                out[0] = p.x; 
                out[1] = p.y; 
                out[2] = p.z; 
                out[3] = p.curvature; 
        } 
}; 
////////////////////////////////////////////////////////////////////////// 
class SimpleOpenNIViewer 
{ 
public: 
        SimpleOpenNIViewer () : viewer ("PCL Viewer") 
        { 

                get_target= false; 
                regis_start = false; 
                
        } 
        
        pcl::PointCloud<pcl::FPFHSignature33>::Ptr getFeaturesFPFH( PointCloud::Ptr cloud, PointCloudWithNormals::Ptr normals, double radius ) 
        {	
                pcl::PointCloud<pcl::FPFHSignature33>::Ptr features = pcl::PointCloud<pcl::FPFHSignature33>::Ptr (new pcl::PointCloud<pcl::FPFHSignature33>); 
                pcl::search::KdTree<PointT>::Ptr search_method_ptr = pcl::search::KdTree<PointT>::Ptr (new pcl::search::KdTree<PointT>); 
                pcl::FPFHEstimation<PointT, PointNormalT, pcl::FPFHSignature33> fpfh_est; 
                fpfh_est.setInputCloud( cloud ); 
                fpfh_est.setInputNormals( normals ); 
                fpfh_est.setSearchMethod( search_method_ptr ); 
                fpfh_est.setRadiusSearch( radius ); 
                fpfh_est.compute( *features ); 


                return features; 
        } 

//////////////////////////////////////////////////////////////////////////////// 
/** \brief Align a pair of PointCloud datasets and return the result 
  * \param cloud_src the source PointCloud 
  * \param cloud_tgt the target PointCloud 
  * \param output the resultant aligned source PointCloud 
  * \param final_transform the resultant transform between source and target 
  */ 
void pairAlign (const PointCloud::Ptr cloud_src, const PointCloud::Ptr cloud_tgt, PointCloud::Ptr output, Eigen::Matrix4f &final_transform) 
{ 
        // 
        // Downsample for consistency and speed 
        // \note enable this for large datasets 
        PointCloud::Ptr src (new PointCloud); 
        PointCloud::Ptr tgt (new PointCloud); 
        pcl::VoxelGrid<PointT> grid; 

                grid.setLeafSize (0.05, 0.05, 0.05); 
                grid.setInputCloud (cloud_src); 
                grid.filter (*src); 

                grid.setInputCloud (cloud_tgt); 
                grid.filter (*tgt); 

        // Compute surface normals and curvature 
        PointCloudWithNormals::Ptr points_with_normals_src (new PointCloudWithNormals); 
        PointCloudWithNormals::Ptr points_with_normals_tgt (new PointCloudWithNormals); 

        pcl::NormalEstimation<PointT, PointNormalT> norm_est; 
        pcl::search::KdTree<PointT>::Ptr tree (new pcl::search::KdTree<PointT> ()); 
        norm_est.setSearchMethod (tree); 
        norm_est.setKSearch (30); 

        norm_est.setInputCloud (src); 
        norm_est.compute (*points_with_normals_src); 
        pcl::copyPointCloud (*src, *points_with_normals_src); 

        norm_est.setInputCloud (tgt); 
        norm_est.compute (*points_with_normals_tgt); 
        pcl::copyPointCloud(*tgt, *points_with_normals_tgt); 

        // 
        // Instantiate our custom point representation (defined above) ... 
        MyPointRepresentation point_representation; 
        // ... and weight the 'curvature' dimension so that it is balanced against x, y, and z 
        float alpha[4] = {1.0, 1.0, 1.0, 1.0}; 
        point_representation.setRescaleValues (alpha); 
        Eigen::Matrix4f Ti = Eigen::Matrix4f::Identity (), targetToSource; 
        
#ifdef FPFH_METHOD 

        double fpfh_radius = 0.3; 
        //Compute FPFH features 
        pcl::PointCloud<pcl::FPFHSignature33>::Ptr model_features= getFeaturesFPFH( src, points_with_normals_src, fpfh_radius ); 
        pcl::PointCloud<pcl::FPFHSignature33>::Ptr frame_features= getFeaturesFPFH( tgt, points_with_normals_tgt, fpfh_radius ); 

        //Initialize alignment method 
        pcl::SampleConsensusInitialAlignment<PointT, PointT, pcl::FPFHSignature33> sac_ia; 
        sac_ia.setInputSource( src ); 
        sac_ia.setSourceFeatures( model_features ); 
        sac_ia.setInputTarget( tgt); 
        sac_ia.setTargetFeatures( frame_features ); 

        //Set parameters for alignment and RANSAC 
        sac_ia.setMaxCorrespondenceDistance( SAC_MAX_CORRESPONDENCE_DIST ); 
        sac_ia.setMinSampleDistance( SAC_MIN_SAMPLE_DIST ); 
        sac_ia.setMaximumIterations( SAC_MAX_ITERATIONS ); 

        //align frame using FPFH features 
        sac_ia.align( *src ); 
        //std::cout << "Done! MSE: " << sac_ia.getFitnessScore() << endl; 

        //Get the transformation 
        Ti = sac_ia.getFinalTransformation(); 
#else 
        // 
        // Align 
        pcl::IterativeClosestPointNonLinear<PointNormalT, PointNormalT> reg; 
        
        // Set the maximum distance between two correspondences (src<->tgt) to 10cm 
        // Note: adjust this based on the size of your datasets 
        reg.setMaxCorrespondenceDistance (0.25);   
        // Set the point representation 
        //reg.setPointRepresentation (boost::make_shared<const MyPointRepresentation> (point_representation)); 

        reg.setInputSource (points_with_normals_src); 
        reg.setInputTarget (points_with_normals_tgt); 
        
        // 
        // Run the same optimization in a loop and visualize the results 

        reg.setMaximumIterations (ICP_MAX_ITERATIONS); 
        reg.setEuclideanFitnessEpsilon(ICP_EUCLIDEAN_FITNESS_EPSILON); 
        reg.setTransformationEpsilon (ICP_TRANSFORMATION_EPSILON); 
        //reg.setRANSACOutlierRejectionThreshold(0.001); 

                // Estimate 

                reg.align (*points_with_normals_src); 
                std::cout << "has converged:" << reg.hasConverged() << " score: " << reg.getFitnessScore() << std::endl; 
                //accumulate transformation between each Iteration 
                Ti = reg.getFinalTransformation ()/**Ti*/; 
#endif 
        // 
        // Get the transformation from target to source 
        targetToSource = Ti.inverse(); 

        // 
        // Transform target back in source frame 
        pcl::transformPointCloud (*cloud_tgt, *output, targetToSource); 

        //*output += *cloud_src; 

        final_transform = targetToSource; 
} 

/* ---[ */ 
    void cloud_cb_ (const PointCloud::ConstPtr &cloud) 
        { 
                                
                if (!viewer.wasStopped()) { 
                        
 	 //viewer.showCloud(cloud); 
 	 if( source->empty()) { 
 	 *source = *cloud; 
 	 /*viewer.showCloud(source);*/ 
 	 } 
  
 	 if (regis_start) 
 	 { 
 	 PointCloud::Ptr target(new PointCloud); 
 	 PointCloud::Ptr temp (new PointCloud); 

  *target = *cloud; 
  pairAlign (source, target, temp, pairTransform); 
  //transform current pair into the global transform 
  pcl::transformPointCloud (*temp, *temp, GlobalTransform); 
                                        *source = *target; 
 	 //update the global transform 
 	 GlobalTransform = pairTransform * GlobalTransform; 
  *result += *temp; 


 	 std::cout<< "Successful .........\n"; 
 	 viewer.showCloud(result); 
        
                } 
        } 
  } 
void run () 
        {	pcl::Grabber* interface = new pcl::OpenNIGrabber(); 

                        result = PointCloud::Ptr(new PointCloud); 
                        source = PointCloud::Ptr(new PointCloud); 

                        GlobalTransform = Eigen::Matrix4f::Identity (); 
                        cout << "Press key 'r' to registration.... "  << ".\n"; 

                        boost::function<void (const PointCloud::ConstPtr &)> f = 
                        boost::bind (&SimpleOpenNIViewer::cloud_cb_, this, _1); 

                interface->registerCallback (f); 
                interface->start (); 
                char c; 
                while (!viewer.wasStopped()) 
                { 
                        /*sleep (1);*/	
                                c = getchar(); 

                                if( c == 'r' ) { 
                                        cout << "Get start Registration.... "  << ".\n"; 
                                        regis_start = true; 
                        } 
                } 

                interface->stop (); 
        } 

        pcl::visualization::CloudViewer viewer; 

        PointCloud::Ptr result ; 
        PointCloud::Ptr source ; 
        Eigen::Matrix4f GlobalTransform; 
        Eigen::Matrix4f pairTransform; 

private: 
        bool get_target; 
        bool regis_start; 
        

}; 


int main (int argc, char** argv) 
{ 

        SimpleOpenNIViewer v; 
        v.run (); 

        return 0; 
}; 
