
template <typename PointType>
class CloudAdministrator{
public:
	typedef pcl::PointCloud<PointType> Cloud;
    typedef typename Cloud::Ptr CloudPtr;
    typedef typename Cloud::ConstPtr CloudConstPtr;
	typedef pcl::PointNormal PointNormalT;
	typedef pcl::PointCloud<PointNormalT> PointCloudWithNormals;

	CloudAdministrator() : source (new Cloud), result (new Cloud)
	{

	}


	void setSource ( const CloudConstPtr& cloud ) 
	{
		source=cloud;
	}
	CloudConstPtr getSource()
	{
		return source;
	}
	void addToResult(const CloudConstPtr& cloud)
	{
		boost::mutex::scoped_lock lock (mtx_result);
		*result=*cloud;	
	}
	CloudPtr getResult()
	{
		boost::mutex::scoped_lock lock (mtx_result);
		return result;
	}
private:
	boost::mutex mtx_result;
	CloudPtr result;
	CloudConstPtr source;
};