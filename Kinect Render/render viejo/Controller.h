#include <boost/thread/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/openni_grabber.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/io/openni_camera/openni_driver.h>
#include <pcl/filters/approximate_voxel_grid.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/console/parse.h>
#include <pcl/common/time.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/icp_nl.h>
#include <pcl/registration/transforms.h>
#include <pcl/features/normal_3d.h>
#include <pcl/surface/mls.h>
#include <pcl/kdtree/kdtree_flann.h>
#include "Viewer.h"
#include "CloudAdministrator.h"

class Controller {

public: 

	Controller(void) : 
	  cloud_ (new pcl::PointCloud<pcl::PointXYZ>)
      , cloud_smoothed_ (new pcl::PointCloud<pcl::PointXYZ>)
		  
	{
		grid_.setLeafSize (0.01,0.01,0.01);
		first=false;
		registerStart=false;
		smoother_.setSearchRadius (0.03);
		//smoother_.setSqrGaussParam (0.0);
		smoother_.setPolynomialFit (false);
		smoother_.setPolynomialOrder (2);
		cloud_smoothed_.reset (new pcl::PointCloud<pcl::PointXYZ>);
		pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ> ());
		smoother_.setSearchMethod (tree);
		
	}

	~Controller(void)
	{
	}


	void cloud_cb_ (const pcl::PointCloud<pcl::PointXYZ>::ConstPtr&  cloud)
    {
  
	  if (!viewer.wasStopped())
	  {
		  set(cloud);
	  }

    }

    void set (const pcl::PointCloud<pcl::PointXYZ>::ConstPtr&  cloud)
    {
      //lock while we set our cloud;
      boost::mutex::scoped_lock lock (mtx_);
	  
      //cloud_  = cloud;
	  cloudAdm.setSource(cloud);
	 

    }



	pcl::PointCloud<pcl::PointXYZ>::Ptr  get ()
    {
      //lock while we swap our cloud and reset it.
      boost::mutex::scoped_lock lock (mtx_);
      pcl::PointCloud<pcl::PointXYZ>::Ptr temp_cloud (new  pcl::PointCloud<pcl::PointXYZ>);
      
	  grid_.setInputCloud (cloudAdm.getSource());
      grid_.filter (*temp_cloud);

	  smoother_.setInputCloud (temp_cloud);
      smoother_.process (*cloud_smoothed_);
	  
	  

      return (cloud_smoothed_);
    }

	bool regis_start;
	void keyboard_callback (const pcl::visualization::KeyboardEvent& event, void* cookie)
    {
		if(event.keyDown() && event.getKeyCode() == 32)
		{
			registerStart=true;
		}
	  
    }
	bool registerStart;
	bool first;
	void run(){
		pcl::Grabber* interface = new pcl::OpenNIGrabber ();
		boost::function<void (const pcl::PointCloud<pcl::PointXYZ>::ConstPtr&)> f = boost::bind (&Controller::cloud_cb_, this, _1);
        boost::signals2::connection c = interface->registerCallback (f);
        
		//viewer.registerKeyboardCallBack(&Controller::keyboard_callback);
		viewer.viewer->registerKeyboardCallback(&Controller::keyboard_callback,*this,(void*)NULL);
		
		interface->start ();
		while(!viewer.wasStopped())
		{
			viewer.spin(100);

			viewer.addCloud(get(),"source",false);

		}
		interface->stop ();
	}
private:
	pcl::VoxelGrid<pcl::PointXYZ> grid_;
	Viewer<pcl::PointXYZRGBA> viewer;
	pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud_;
	boost::mutex mtx_;
	CloudAdministrator<pcl::PointXYZ> cloudAdm;
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_smoothed_;
	pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointXYZ> smoother_;


};