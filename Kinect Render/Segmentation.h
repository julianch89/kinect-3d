#include "typedef.h"

#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/extract_indices.h>

void segmentPointCloud (PointCloudPtr cloud_filtered)
{
	 pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
     pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());

	 pcl::SACSegmentation<PointT> seg;
	// Optional
	seg.setOptimizeCoefficients (true);
	// Mandatory
	seg.setModelType (pcl::SACMODEL_PLANE);
	seg.setMethodType (pcl::SAC_RANSAC);
	seg.setMaxIterations (1000);
	seg.setDistanceThreshold (0.01);

	pcl::ExtractIndices<PointT> extract;
	
	// Segment the largest planar component from the remaining cloud
	seg.setInputCloud (cloud_filtered);
	seg.segment (*inliers, *coefficients);
	
	PointCloudPtr cloud_p (new PointCloud);
	
	extract.setInputCloud (cloud_filtered);
    extract.setIndices (inliers);
    extract.setNegative (false);
    extract.filter (*cloud_p);

	PointCloudPtr cloud_f (new PointCloud);
	// Create the filtering object
	extract.setNegative (true);
	extract.filter (*cloud_f);
	*cloud_filtered=*cloud_f;

	
}