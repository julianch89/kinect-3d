#pragma once
#include "Filter.h"
#include <list>

template <typename PointType>
class FilterFrames : public Filter <PointType>
{
public:
		
	
	FilterFrames(void)
	{
		
	}

	~FilterFrames(void)
	{
	}
	list<CloudPtr> clouds;

	void addCloud(CloudPtr cloud)
	{
		clouds.push_front(cloud);
	}
	int method;
	void aplyFilter(CloudPtr cloud)
	{
		int h=cloud->height;
		int w=cloud->width;
		CloudPtr cloudAux(new Cloud);
		cloudAux->width=w;
		cloudAux->height=h;
		cloudAux->is_dense = false;
		cloudAux->points.resize (cloud->width * cloud->height);
			
		for (int y=0; y<h; y++)
		{
			for(int x=0;x<w;x++)
			{
				double suma = 0;
				double cantframes = 0;
				for (std::list<CloudPtr>::iterator it = clouds.begin(); it != clouds.end(); it++)
				{
					PointType p = (*it)->at(x,y);
					if(pcl_isfinite(p.z)) 
					{
						suma+= p.z;
						cantframes++;
					}

				}
				cloudAux->at(x,y)=cloud->at(x,y);
				if(cantframes!=0 ) cloudAux->at(x,y).z=suma/cantframes;

			}
		}

		*cloud=*cloudAux;
		clouds.clear();

	}
};