#pragma once
#include "Filter.h"


template <typename PointType>
class FilterZeros : public Filter <PointType>
{
public:
	double  filterCollection  [24][2];	
	int innerBandThreshold;
    int outerBandThreshold;
	FilterZeros(int it=2, int ot=5)
	{
		innerBandThreshold = it;
		outerBandThreshold = ot;
	}

	~FilterZeros(void)
	{
	}
	
	void aplyFilter(CloudPtr cloud)
	{
		int h=cloud->height;
		int w=cloud->width;
		CloudPtr cloudAux(new Cloud);
		cloudAux->width=w;
		cloudAux->height=h;
		cloudAux->is_dense = false;
		cloudAux->points.resize (cloud->width * cloud->height);
			
		for (int y=0; y<h; y++)
		{
			for(int x=0;x<w;x++)
			{
				PointType p= cloud->at(x,y);
					
				if(!pcl_isfinite(p.z) )
				{
					
                    for (int i = 0; i < 24; i++)
                    {
                        filterCollection[i][0] = 0;
                        filterCollection[i][1] = 0;
                    }

                    int innerBandCount = 0;
                    int outerBandCount = 0;

					for (int yi = -2; yi < 3; yi++)
                        {
                            for (int xi = -2; xi < 3; xi++)
                            {
                                int xSearch = x + xi;
                                int ySearch = y + yi;

                                if (xSearch >= 0 && xSearch < w && ySearch >= 0 && ySearch < h)
                                {
									PointType pAux= cloud->at(xSearch,ySearch);
                                    if (pcl_isfinite(pAux.z))
                                    {
                                        for (int i = 0; i < 24; i++)
                                        {
                                            if (filterCollection[i][0] == pAux.z)
                                            {
                                                // When the depth is already in the filter collection
                                                // we will just increment the frequency.
                                                filterCollection[i][1]++;
                                                break;
                                            }
                                            else if (filterCollection[i][0] == 0)
                                            {
                                                // When we encounter a 0 depth in the filter collection
                                                // this means we have reached the end of values already counted.
                                                // We will then add the new depth and start it's frequency at 1.
                                                filterCollection[i][0] = pAux.z;
                                                filterCollection[i][1]++;
                                                break;
                                            }
                                        }

                                        if (yi != 2 && yi != -2 && xi != 2 && xi != -2)
                                            innerBandCount++;
                                        else
                                            outerBandCount++;
                                    }
                                }

                            }
                        }
					    cloudAux->at(x,y)=cloud->at(x,y);
                        if (innerBandCount >= innerBandThreshold || outerBandCount >= outerBandThreshold)
                        {
                            double frequency = 0;
                            double depth = 0;
                            double count = 0;
                            // This loop will determine the statistical mode
                            // of the surrounding pixels for assignment to
                            // the candidate.
                            for (int i = 0; i < 24; i++)
                            {
                                // This means we have reached the end of our
                                // frequency distribution and can break out of the
                                // loop to save time.
                                if (filterCollection[i][0] == 0)
                                    break;
                               // if (filterCollection[i, 1] > frequency)
                               // {
                                    depth += filterCollection[i][0];
                                    count++;
                                    frequency = filterCollection[i][1];
                                //}
                            }
							cloudAux->at(x,y).z= depth/count;
						}

					
					
					
				}
				else
				{
					cloudAux->at(x,y)=cloud->at(x,y);
				}
					
			}
		}
		*cloud=*cloudAux;

	 }
};