#pragma once
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/surface/organized_fast_mesh.h>
#include <pcl/surface/gp3.h>
#include <pcl/surface/grid_projection.h>
#include <pcl/io/vtk_io.h>

using pcl::visualization::PointCloudColorHandlerGenericField;
using pcl::visualization::PointCloudColorHandlerCustom;
template <typename PointType>
class Viewer
{
public:
	typedef pcl::PointCloud<PointType> Cloud;
    typedef typename Cloud::Ptr CloudPtr;
    typedef typename Cloud::ConstPtr CloudConstPtr;

	typedef pcl::FPFHSignature33 LocalDescriptorT;
	typedef pcl::PointCloud<LocalDescriptorT> LocalDescriptors;
	typedef pcl::PointCloud<LocalDescriptorT>::Ptr LocalDescriptorsPtr;
	typedef pcl::PointCloud<LocalDescriptorT>::ConstPtr LocalDescriptorsConstPtr;
	typedef pcl::Normal NormalT;
	typedef pcl::PointCloud<NormalT> SurfaceNormals;
	typedef pcl::PointCloud<NormalT>::Ptr SurfaceNormalsPtr;
	typedef pcl::PointCloud<NormalT>::ConstPtr SurfaceNormalsConstPtr;
	typedef pcl::PointNormal PointNormalT;
	typedef pcl::PointCloud<PointNormalT> PointCloudWithNormals;
	typedef pcl::PointCloud<PointNormalT>::Ptr PointCloudWithNormalsPtr;
	typedef pcl::PointCloud<PointNormalT>::ConstPtr PointCloudWithNormalsConstPtr;

	Viewer(void): viewer  (new pcl::visualization::PCLVisualizer ("Render Kinect"))
	{
		

		viewer->createViewPort (0.0, 0.0, 0.5, 1.0, viewport_input_);
        viewer->setBackgroundColor (0.3, 0.3, 0.3, viewport_input_);
		
        viewer->createViewPort (0.5, 0.0, 1.0, 1.0, viewport_result_);
        viewer->setBackgroundColor (0.3, 0.3, 0.3, viewport_result_);
		
		viewer->initCameraParameters(); 
		viewer->addCoordinateSystem (0.3);
		viewer->addText("Live cloud from Kinect sensor",10,10,14,1,1,1,"text",viewport_input_);
		color=false;

	}
	~Viewer(void)
	{

	}


	void spin(int n)
	{
		viewer->spinOnce(n);
	}

	bool wasStopped()
	{
		return viewer->wasStopped();
	}

	void updateColorCloud(CloudConstPtr c,string name) 
	{
		if(!viewer->wasStopped())
		{
			pcl::visualization::PointCloudColorHandlerCustom<PointType> single_color(c, 0, 255, 0);
			viewer->removePointCloud(name,viewport_result_);
			if(!color)
			{	
				viewer->addPointCloud(c,single_color,name,viewport_result_);
			}
			else
			{
			    viewer->addPointCloud(c,name,viewport_result_);
			}
		}
	}

	void addCloud(CloudConstPtr c,string name, bool result )
	{
		pcl::visualization::PointCloudColorHandlerCustom<PointType> single_color(c, 0, 255, 0);
		
		if(!viewer->wasStopped())
			if(!result)
			{
				if(!viewer->updatePointCloud(c,name))
					viewer->addPointCloud(c,name,viewport_input_);
			}
			else
			{
				if(!color)
				{
					if(!viewer->updatePointCloud(c,single_color,name))
						viewer->addPointCloud(c,single_color,name,viewport_result_);
				}
				else
				{
					if(!viewer->updatePointCloud(c,name))
						viewer->addPointCloud(c,name,viewport_result_);
				}
			}
			/*
			if(!viewer->updatePointCloud(c,name))
				if(!result) viewer->addPointCloud(c,name,viewport_input_);
				else viewer->addPointCloud(c,name,viewport_result_);
			*/	
				
	}
	void addKeyPoint (CloudConstPtr kpoints,string name ) 
	{
		pcl::visualization::PointCloudColorHandlerCustom<PointType> key_color(kpoints, 255, 0, 0);
		
		if(!viewer->wasStopped())
		{
			if(!viewer->updatePointCloud(kpoints,key_color,name))
					viewer->addPointCloud(kpoints,key_color,name,viewport_result_);
			 viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, name); 
			 
		}
	}
	void removeCloud(string name)
	{
		if(!viewer->wasStopped())
		{
			viewer->removePointCloud(name,viewport_result_);
		}
	}

	void addNormalCloud (CloudConstPtr cloud , PointCloudWithNormalsPtr normals)
	{
		if(!viewer->wasStopped())
		{
			viewer->addPointCloudNormals<PointType, pcl::PointNormal>(cloud,normals,100,0.01,"normals",viewport_result_);
			
		}
	}
	
	void addTextResult(string text,int xpos , int ypos) 
	{
		if(!viewer->wasStopped())
		{
			viewer->addText(text,xpos,ypos,11,1,1,1,"text",viewport_result_);
			
		}
	}
	
	void setColor (bool c)
	{
		color=c;
	}

	void removeMesh()
	{
		if(!viewer->wasStopped())
			viewer->removePolygonMesh("surface");
	}

	void addMeshGrid(CloudPtr cloud)
	{
	    viewer->removePointCloud("result");
		pcl::search::KdTree<PointType>::Ptr tree3 (new pcl::search::KdTree<PointType>);

		// Output has the PointNormal type in order to store the normals calculated by MLS
		CloudPtr mls_points (new Cloud);

		// Init object (second point type is for the normals, even if unused)
		pcl::MovingLeastSquares<PointType, PointType> mls;
 
		mls.setComputeNormals (true);

		// Set parameters
		mls.setInputCloud (cloud);
		mls.setPolynomialFit (true);
		mls.setSearchMethod (tree3);
		mls.setSearchRadius (0.03);

		// Reconstruct
		mls.process (*mls_points);
		pcl::NormalEstimation<PointType, PointNormalT> n;
		//pcl::PointCloud<pcl::Normal>::Ptr normals (new pcl::PointCloud<pcl::Normal>);
		pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals (new pcl::PointCloud<pcl::PointNormal>);
		pcl::search::KdTree<PointType>::Ptr tree (new pcl::search::KdTree<PointType>);
		tree->setInputCloud (mls_points);
		n.setInputCloud (mls_points);
		n.setSearchMethod (tree);
		n.setKSearch (20);
		n.compute (*cloud_with_normals);

		
		
		pcl::copyPointCloud (*mls_points, *cloud_with_normals);

		pcl::search::KdTree<pcl::PointNormal>::Ptr tree2 (new pcl::search::KdTree<pcl::PointNormal>); 
        tree2->setInputCloud (cloud_with_normals); 

        // Initialize objects	
        pcl::GridProjection<pcl::PointNormal> gbpolygon; 
        pcl::PolygonMesh triangles; 

        // Set parameters 
        gbpolygon.setResolution(0.005); 
        gbpolygon.setPaddingSize(3); 
        gbpolygon.setNearestNeighborNum(20); 
        gbpolygon.setMaxBinarySearchLevel(8); 
        
        // Get result 
        gbpolygon.setInputCloud(cloud_with_normals); 
        gbpolygon.setSearchMethod(tree2); 
        gbpolygon.reconstruct(triangles); 
		if(!viewer->wasStopped())
		{
			    viewer->removePolygonMesh("surface");
				//pcl::visualization::PointCloudColorHandlerCustom<PointType> single_color(mls_points, 0, 255, 0);
				//if(!viewer->updatePointCloud(mls_points,single_color,"smooth"))
					//	viewer->addPointCloud(mls_points,single_color,"smooth",viewport_result_);
				viewer->addPolygonMesh(triangles ,"surface",viewport_result_);
		}
		
		//pcl::io::saveVTKFile ("mesh.vtk", triangles);
	}
	void addMesh(CloudPtr cloud )
	{
		viewer->removePointCloud("result");
		/*
		ofm.setTrianglePixelSize (3);
        ofm.setTriangulationType (pcl::OrganizedFastMesh<PointType>::QUAD_MESH);
		ofm.setInputCloud (cloud);
		boost::shared_ptr<std::vector<pcl::Vertices> > temp_verts (new std::vector<pcl::Vertices>);
		ofm.reconstruct (*temp_verts);
		*/
		
		pcl::search::KdTree<PointType>::Ptr tree3 (new pcl::search::KdTree<PointType>);

		// Output has the PointNormal type in order to store the normals calculated by MLS
		CloudPtr mls_points (new Cloud);

		// Init object (second point type is for the normals, even if unused)
		pcl::MovingLeastSquares<PointType, PointType> mls;
 
		mls.setComputeNormals (true);

		// Set parameters
		mls.setInputCloud (cloud);
		mls.setPolynomialFit (true);
		mls.setSearchMethod (tree3);
		mls.setSearchRadius (0.03);

		// Reconstruct
		mls.process (*mls_points);


		pcl::NormalEstimation<PointType, PointNormalT> n;
		//pcl::PointCloud<pcl::Normal>::Ptr normals (new pcl::PointCloud<pcl::Normal>);
		pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals (new pcl::PointCloud<pcl::PointNormal>);
		pcl::search::KdTree<PointType>::Ptr tree (new pcl::search::KdTree<PointType>);
		tree->setInputCloud (mls_points);
		n.setInputCloud (mls_points);
		n.setSearchMethod (tree);
		n.setKSearch (20);
		n.compute (*cloud_with_normals);

		// Concatenate the XYZ and normal fields*
		
		pcl::copyPointCloud (*mls_points, *cloud_with_normals);
		//* cloud_with_normals = cloud + normals

		pcl::search::KdTree<pcl::PointNormal>::Ptr tree2 (new pcl::search::KdTree<pcl::PointNormal>);
        tree2->setInputCloud (cloud_with_normals);

		pcl::PolygonMesh triangles;
		gp3.setSearchRadius (0.025);

		// Set typical values for the parameters
		
		gp3.setMu (2.5);
		//gp3.setMaximumNearestNeighbors (20);
		//gp3.setMaximumSurfaceAngle(M_PI/4); // 45 degrees
		//gp3.setMinimumAngle(M_PI/18); // 10 degrees
		//gp3.setMaximumAngle(4*M_PI/3); // 120 degrees
		gp3.setNormalConsistency(false);

		// Get result
		gp3.setInputCloud (cloud_with_normals);
		gp3.setSearchMethod (tree2);
		gp3.reconstruct (triangles);

		// Additional vertex information
		//std::vector<int> parts = gp3.getPartIDs();
		//std::vector<int> states = gp3.getPointStates();

		
		if(!viewer->wasStopped())
		{
			    viewer->removePolygonMesh("surface");
				//pcl::visualization::PointCloudColorHandlerCustom<PointType> single_color(mls_points, 0, 255, 0);
				//if(!viewer->updatePointCloud(mls_points,single_color,"smooth"))
						//viewer->addPointCloud(mls_points,single_color,"smooth",viewport_result_);
				viewer->addPolygonMesh(triangles ,"surface",viewport_result_);
		}
	}
	int viewport_input_, viewport_result_;
	pcl::OrganizedFastMesh<PointType> ofm;
	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
	pcl::GreedyProjectionTriangulation<pcl::PointNormal> gp3;
	bool color;
};

