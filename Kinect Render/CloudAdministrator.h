#include <pcl/io/pcd_io.h>

#include "FilterSmooth.h"
#include "FilterGeneral.h"
#include "FilterZeros.h"
#include "FilterHSV.h"
#include "FilterFrames.h"
#include "CloudFeatures.h"
#include "Segmentation.h"
#include "Registration.h"
#include "typedef.h"


#include <boost/make_shared.hpp>
#include <pcl/point_cloud.h>
#include <pcl/point_representation.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/filter.h>
#include <pcl/features/normal_3d.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/icp_nl.h>
#include <pcl/registration/transforms.h>
#include <pcl/features/pfh.h>
#include <pcl/features/fpfh.h>
#include "pcl/kdtree/kdtree_flann.h" 
#include <pcl/filters/passthrough.h>
#include <pcl/registration/ia_ransac.h>
#include <pcl/filters/statistical_outlier_removal.h>




// Filter Parameters
float min_depth = 0.2;
float max_depth = 2.0;
float leaf_size = 0.005;
float radius = 0.05;
float min_neighbors = 8;

// Pose Estimation Parameters
CloudFeatures src_features;
CloudFeatures src_features_aux;
float min_sample_dist = 0.05;
float max_correspondence_dist = 0.1;
int nr_iters = 500;

// ICP Paramenters
float max_correspondence_distance = 0.1;
float outlier_rejection_threshold = 0.05;
float transformation_epsilon = 1e-8;
int max_iterations = 100;



template <typename PointType>
class CloudAdministrator{
public:
	typedef pcl::PointCloud<PointType> Cloud;
    typedef typename Cloud::Ptr CloudPtr;
    typedef typename Cloud::ConstPtr CloudConstPtr;


	CloudAdministrator() : source (new Cloud), result (new Cloud) , target(new Cloud),live(new Cloud) ,keypoints(new Cloud),normals(new PointCloudWithNormals), result_backup(new Cloud)
	{
		GlobalTransform = Eigen::Matrix4f::Identity (); 
		cantclouds=0;
		first=true;
	}


	void setSource ( const CloudConstPtr& cloud ) 
	{
		boost::mutex::scoped_lock lock (mtx_source);
		*live=*cloud;
	}
	CloudConstPtr getSource()
	{
		boost::mutex::scoped_lock lock (mtx_source);
		return live;
	}
	CloudConstPtr getKeyPoints()
	{
		return keypoints;
	}
	PointCloudWithNormalsPtr getNormals()
	{
		return normals;
	}
	int cantclouds;
	void removeLast()
	{
		if(!first)
		{
			*result = *result_backup;
			src_features = src_features_aux;
		}

	}

	void addFrame(const CloudConstPtr& cloud)
	{
		boost::mutex::scoped_lock lock (mtx_result);
		PointCloudPtr cloud_;
		cloud_.reset(new PointCloud());
		pcl::copyPointCloud(*cloud,*cloud_);
		filFrames.addCloud(cloud_);
	}

	void addToResult(const CloudConstPtr& cloud)
	{
		/*
		cantclouds++;
		std::stringstream ss;
        ss << "result" << cantclouds << ".pcd";
		pcl::io::savePCDFile (ss.str(), *cloud, true);
		*/
		boost::mutex::scoped_lock lock (mtx_result);
		PointCloudPtr cloud_;
		cloud_.reset(new PointCloud());
		pcl::copyPointCloud(*cloud,*cloud_);

		printf ("Original Points: %i \n" ,cloud_->points.size() );

		// Frames Filter
		//filFrames.aplyFilter(cloud_);
		
		// HSV Filter
		//FilterHSV<PointType> filHSV();
		//filHSV.aplyFilter(cloud_);

		// Smooth filter
		FilterSmooth<PointType> fil ;
		//fil.aplyFilter(cloud_);

		// Zeros Filter
		FilterZeros<PointType> fil2;
		//fil2.aplyFilter(cloud_);
		//fil.aplyFilter(cloud_);

		// General Filter 
		FilterGeneral<PointType> fil1 (min_depth,max_depth,leaf_size,radius,min_neighbors);
		
 		fil1.aplyFilter(cloud_);

		printf ("Filter general: %i \n",cloud_->points.size() );

		//segmentPointCloud(cloud_);
		printf ("Segmentations Points: %i \n" ,cloud_->points.size() );

		//std::vector<int> indices;
        //pcl::removeNaNFromPointCloud(*cloud_,*cloud_, indices);
		CloudFeatures tgt_features;
		// Points, Normals, Keypoints and Descriptors
		tgt_features = computeFeatures(cloud_);

		printf ("Filtered Points: %i \n" ,tgt_features.points->points.size());

		printf ("Keypoints: %i \n"  ,tgt_features.keypoints->points.size());

		printf ("Normals: %i \n" ,tgt_features.normals->points.size());

		printf ("Result: %i \n" ,result->points.size());

		printf ("Result Keypoints: %i \n" ,keypoints->points.size());

		printf ("Result Normals: %i \n" ,normals->points.size());

		//*target=*cloud_;
		 
		if(first) 
		{

			*result+=*cloud_;
			*keypoints+=*tgt_features.keypoints;
			//*normals+=*tgt_features.normals;
			first=false;
		}
		else
		{


			
			Eigen::Matrix4f tform = Eigen::Matrix4f::Identity();
			// Compute Initial Alignment
			tform = computeInitialAlignment(src_features.keypoints, src_features.local_descriptors, tgt_features.keypoints, tgt_features.local_descriptors, min_sample_dist, max_correspondence_dist, nr_iters);

			// Use ICP with Initial Alignment
			tform = refineAlignment(src_features.points, tgt_features.points, tform, max_correspondence_distance, outlier_rejection_threshold, transformation_epsilon, max_iterations);
			

			pcl::io::savePCDFile ("src_pre", *src_features.points, true);
			pcl::transformPointCloud (*src_features.points, *src_features.points, tform);
			pcl::io::savePCDFile ("src_final", *src_features.points, true);
			pcl::io::savePCDFile ("tgt_final", *tgt_features.points, true);
			/*
			// Add to Global Map
			Eigen::Matrix4f relative_tform = tform.inverse();
			GlobalTransform = GlobalTransform * relative_tform;

			pcl::PointCloud<PointType> transformedCloud;
			pcl::PointCloud<PointType> transformedKeys;
			PointCloudWithNormals transformedNormals;
			pcl::transformPointCloud (*tgt_features.points, transformedCloud, GlobalTransform);
			pcl::transformPointCloud (*tgt_features.keypoints, transformedKeys, GlobalTransform);
			//pcl::transformPointCloud (*tgt_features.normals, transformedNormals, GlobalTransform);
			//pcl::transformPointCloud(*tgt_features.normals,transformedNormals,GlobalTransform);
			
			//pcl::transformPointCloudWithNormals(*tgt_features.normals,transformedNormals,GlobalTransform);
			*keypoints+=transformedKeys;
		    //*normals+=transformedNormals;
			*result_backup=*result;
			*result += transformedCloud; 
			*/
		}
		src_features_aux = src_features;
		src_features = tgt_features;
		
		
	}


    
	CloudPtr getResult()
	{
		boost::mutex::scoped_lock lock (mtx_result);
		return result;
	}
	

private:
	boost::mutex mtx_source;
	boost::mutex mtx_result;
	CloudPtr result;
	CloudPtr result_backup;
	CloudPtr source;
	CloudPtr live;
	CloudPtr target;
	bool first;
	Eigen::Matrix4f GlobalTransform;
	Eigen::Matrix4f pairTransform;
	CloudPtr keypoints;
	PointCloudWithNormalsPtr  normals;
    FilterFrames<PointType> filFrames;

	
};