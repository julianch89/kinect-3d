#pragma once
#include "Filter.h"
#include "HSV.h"

template <typename PointType>
class FilterHSV : public Filter <PointType>
{
public:
		
	double min;
	double max;
	FilterHSV(double Min , double Max)
	{
		min=Min;
		max=Max;
	}

	~FilterHSV(void)
	{
	}
	int method;
	void aplyFilter(CloudPtr cloud)
	{
		int h=cloud->height;
		int w=cloud->width;
		CloudPtr cloudAux(new Cloud);
		cloudAux->width=w;
		cloudAux->height=h;
		cloudAux->is_dense = false;
		cloudAux->points.resize (cloud->width * cloud->height);
			
		for (int y=0; y<h; y++)
		{
			for(int x=0;x<w;x++)
			{
				if(pcl_isfinite(p.z) )
				{
					rgb color;
					
					color.r = cloud->at(x,y).r;
					color.g = cloud->at(x,y).g;
					color.b = cloud->at(x,y).b;
					hsv hue = RGBtoHSV(color);
					if(hsv.h <= min || hsv.h >= max ) cloudAux->at(x,y) = cloud->at(x,y);
				}
			}
		}
		*cloud=*cloudAux;
		
	}
};