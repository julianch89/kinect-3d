#include <boost/thread/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/openni_grabber.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/io/openni_camera/openni_driver.h>
#include <pcl/filters/approximate_voxel_grid.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/console/parse.h>
#include <pcl/common/time.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/icp_nl.h>
#include <pcl/registration/transforms.h>
#include <pcl/features/normal_3d.h>
#include <pcl/surface/mls.h>
#include <pcl/kdtree/kdtree_flann.h>
#include "Viewer.h"
#include "CloudAdministrator.h"
#include <pcl/io/vtk_io.h>
class Controller {

public: 

	Controller(void) 
	
		  
	{
		first=false;
		registerStart=false;
		result_change=false;
		frames=0;
		keypoints=false;
		normals=false;
		mesh=false;
		color=false;
		maxFrames=5;
	}
	int frames;
	int maxFrames;
	~Controller(void)
	{
	}

	bool result_change;
	void cloud_cb_ (const pcl::PointCloud<pcl::PointXYZRGBA>::ConstPtr&  cloud)
    {

		
		cloudAdm.setSource(cloud);
	
		if(registerStart)
		{
			//if(frames>maxFrames){
				
				cloudAdm.addToResult(cloud);
				result_change=true;
				registerStart=false;
				//frames=0;
			//}
			//else
		//	{
			//	cloudAdm.addFrame(cloud);
		//	}
			//frames++;
			
		}
		
    }
	
	pcl::PointCloud<pcl::PointXYZRGBA>::Ptr  getResult ()
    {
      
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr temp_cloud (new  pcl::PointCloud<pcl::PointXYZRGBA>);

	  *temp_cloud=*cloudAdm.getResult();
      
	  return (temp_cloud);

    }


	pcl::PointCloud<pcl::PointXYZRGBA>::Ptr  get ()
    {

      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr temp_cloud (new  pcl::PointCloud<pcl::PointXYZRGBA>);

	  *temp_cloud=*cloudAdm.getSource();
	  
	  return (temp_cloud);

    }


	void keyboard_callback (const pcl::visualization::KeyboardEvent& event, void* cookie)
    {
		
		if(event.keyDown() && event.getKeyCode() == 32)
		{
			std::cout << event.getKeySym() <<" was pressed " << std::endl;
			registerStart=!registerStart;
			
		}
		if(event.keyDown() && event.getKeySym()== "a")
		{
			std::cout << event.getKeySym() <<" was pressed " << std::endl;
			keypoints=!keypoints;
			if(keypoints)viewer.addKeyPoint(cloudAdm.getKeyPoints(),"keypoints");
			else viewer.removeCloud("keypoints");
			
			
		}
		if(event.keyDown() && event.getKeySym()== "r")
		{
			cloudAdm.removeLast();
			result_change=true;
			std::cout << "remove all." << std::endl;
			
		}
		if(event.keyDown() && event.getKeySym()== "n")
		{
			normals=!normals;
			std::cout << event.getKeySym() <<" was pressed " << std::endl;
			if(normals)viewer.addNormalCloud(cloudAdm.getResult(),cloudAdm.getNormals());
			else viewer.removeCloud("normals");
			
		}
		if(event.keyDown() && event.getKeySym()== "m")
		{
			
			std::cout << event.getKeySym() <<" was pressed " << std::endl;
			mesh=!mesh;
			if(mesh)viewer.addMeshGrid(cloudAdm.getResult());
			else viewer.removeMesh();
			
		}
		
		if(event.keyDown() && event.getKeySym()== "k")
		{
			
			std::cout << event.getKeySym() <<" was pressed " << std::endl;
			mesh=!mesh;
			if(mesh)viewer.addMesh(cloudAdm.getResult());
			else viewer.removeMesh();
			
		}

		if(event.keyDown() && event.getKeySym()== "c")
		{
			
			std::cout << event.getKeySym() <<" was pressed " << std::endl;
			color=!color;
			viewer.setColor(color);
			viewer.updateColorCloud(getResult(),"result");
			
		}
		if(event.keyDown() && event.getKeySym()== "s")
		{
			pcl::io::savePCDFile("outputKinect.pcd",*cloudAdm.getResult());

		}

	  
    }
	bool registerStart;
	bool first;
	void run(){
		pcl::Grabber* interface = new pcl::OpenNIGrabber ();
		
		boost::function<void (const pcl::PointCloud<pcl::PointXYZRGBA>::ConstPtr&)> f = boost::bind (&Controller::cloud_cb_, this, _1);
        boost::signals2::connection c = interface->registerCallback (f);
        
		viewer.viewer->registerKeyboardCallback(&Controller::keyboard_callback,*this,(void*)NULL);
		
		interface->start ();
		while(!viewer.wasStopped())
		{
			
			viewer.addCloud(get(),"cloud",false);
			
			
			if(result_change)
			{
				viewer.addCloud(getResult(),"result",true);
				result_change=false;
			
			}
			
			
			viewer.spin(100);
				
		}
		interface->stop ();
	}

private:

	Viewer<pcl::PointXYZRGBA> viewer;

	boost::mutex mtx_;
	boost::mutex mtx_2;
	CloudAdministrator<pcl::PointXYZRGBA> cloudAdm;
	bool keypoints;
	bool normals;
	bool mesh;
	bool color;


};